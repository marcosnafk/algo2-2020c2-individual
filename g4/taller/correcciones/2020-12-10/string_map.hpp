
#include "string_map.h"

//template <class t1, typename T>
template <typename T>
string_map<T>::string_map():_raiz(new Nodo()),_tamano(0){ }

/*
    Es un nodo con un vector de punteros, cada pos es un char del ascci, entonces escribir arbol es de la raiz ir a su letra "a" y cambiar el null por new nodo, y a ese
    new nodo vas a la pos b y asi... llegas  al ultimo nodo, la letra "l", new nodo y a ese new nodo solamente le metes el significado
 */

template< typename T>
void string_map<T>::insert(const pair<string,T>& palabra) {

    string clave = palabra.first; /// fijarse si anda usando " = &palabra.first;"
    Nodo* aux = _raiz;

    for(int i =0; i < clave.size() ; i++ ){

        char letra = clave[i];

        if ( aux->siguientes[int(letra)] != nullptr )
            aux = aux->siguientes[int(letra)];
        else{
            aux->siguientes[int(letra)] = new Nodo;
            aux = aux->siguientes[int(letra)];
        }
    }
    if ( aux->definicion == nullptr)
        _tamano++;

    //borar anterior definicion
    if ( aux->definicion != nullptr )
        delete aux->definicion;

    T* res = new T(palabra.second);

    aux->definicion = res;
}




template <typename T>
int string_map<T>::count(const string& clave) const{
    // Buscar la clave
    // 1 si esta, 0 si no
    Nodo* aux = _raiz;
    bool podriaEstar = true;

    for(int i =0; i < clave.size() && podriaEstar ; i++ ){

        char letra = clave[i];

        if ( aux->siguientes[int(letra)] != nullptr )
            aux = aux->siguientes[int(letra)];
        else{
            podriaEstar = false;
        }
    }
    int res = 0;

    if ( podriaEstar && aux->definicion != nullptr )
        res = 1;

    return res;

}

template <typename T>
const T& string_map<T>::at(const string& clave) const {
    // Buscar con la clave
    // Return el significado
    Nodo* aux = _raiz;

    for(int i =0; i < clave.size()  ; i++ ){
        char letra = clave[i];
        aux = aux->siguientes[int(letra)];
    }

    return aux->definicion;
}

template <typename T>
T& string_map<T>::at(const string& clave) {
    // Buscar con la clave
    // Return el significado
    Nodo* aux = _raiz;

    for(int i =0; i < clave.size()  ; i++ ){
        char letra = clave[i];
        aux = aux->siguientes[int(letra)];
    }




    return *aux->definicion; /// esta esperando a el valor en si mismo, aux->definicion sin el * es la direccion de memoria, *aux->definicion es el elemento de tipo T
}

template <typename T>
void string_map<T>::erase(const string& clave) {
    // Buscar
    // Elimminar
    // Restar 1 a _tamano
    Nodo* aux = _raiz;

    for(int i =0; i < clave.size()  ; i++ ){
        char letra = clave[i];
        aux = aux->siguientes[int(letra)];
    }
    delete aux->definicion;
    aux->definicion = nullptr;  /// habria que ir eliminando todos los nodos anteriores si no apuntaban a nada ie dicc con carpa y calor, borramos
                                /// carpa los nodos de r p y a deberian ser borrados, PEEERO bueno, si anda anda, otro dia hago el nodo con padre y los borro mas facilmente
    _tamano --;

}

template <typename T>
int string_map<T>::size() const{
    return _tamano;
}

template <typename T>
bool string_map<T>::empty() const{
    return _tamano == 0;
}

template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() {


    *this = aCopiar;
} // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template <typename T>
string_map<T>& string_map<T>::operator=(const string_map<T>& d) {


    for (int i = 0; i < this->_raiz->siguientes.size() ; ++i) {
        if ( this->_raiz->siguientes[i] != nullptr )
            seekAndDestroy(this->_raiz->siguientes[i]);
    }

    for (int i = 0; i < this->_raiz->siguientes.size() ; ++i) {
        _raiz->siguientes[i]= nullptr;
    }
    delete _raiz->definicion;


    _tamano = d._tamano;

    for (int j = 0; j < d._raiz->siguientes.size() ; ++j) {
        if ( d._raiz->siguientes[j] != nullptr ){

            Nodo* aux = new Nodo();
            copiarNodo(aux,d._raiz->siguientes[j] );
            _raiz->siguientes[j] = aux;
        }
    }
/*
    int* aux = new int(*d->definicion);

    _raiz->definicion = aux;



    T* aux = new T;
    *aux = *d->definicion;
    */

    /* esto no anda
     * */
    if  ( d._raiz->definicion != nullptr ) {
        T *defAux = new T(*(d._raiz->definicion));
        _raiz->definicion = defAux;
    }


    return *this;

}

template<typename  T>
void string_map<T>::copiarNodo(Nodo* copia, Nodo* aCopiar) {
    for (int i = 0; i < aCopiar->siguientes.size(); ++i) {
        if ( aCopiar->siguientes[i] != nullptr ){
            Nodo* aux = new Nodo();
            copiarNodo(aux, aCopiar->siguientes[i]);
            copia->siguientes[i] = aux;

        }

    }

    /* no anda
    T* aux = new T;
    *aux = *aCopiar->definicion;
    copia->definicion = aux;
    */
    //T* defAux = new T(*(aCopiar->definicion));

    if ( aCopiar->definicion != nullptr ) {
        T *aux = new T(*aCopiar->definicion);
        copia->definicion = aux;
    }
}




template <typename T>
string_map<T>::~string_map(){
    // un for que recorra cada puntero de la raiz
    // recursion sobre cada puntero si no es null
    // delete raiz
    // misma idea para los otros
    for (int i = 0; i < _raiz->siguientes.size() ; ++i) {
        if ( _raiz->siguientes[i] != nullptr )
            seekAndDestroy(_raiz->siguientes[i]);
    }
    delete _raiz->definicion;
    delete _raiz;
}

template<typename T>
void string_map<T>::seekAndDestroy(Nodo * elem) {

    for (int i = 0; i < elem->siguientes.size() ; ++i) {
        if ( elem->siguientes[i] != nullptr )
            seekAndDestroy(elem->siguientes[i]);
    }
    delete elem->definicion;
    delete elem;
}

template <typename T>
T& string_map<T>::operator[](const string& clave){
    // COMPLETAR
}


