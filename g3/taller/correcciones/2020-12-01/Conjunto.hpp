
#include "Conjunto.h"

/*
 Una lista de padres? Para poder ver el padre de mi padre y mas allá
 porque algunos .siguiente hay que ponerlos como ->siguiente ?
 Aux's para
    -padre
    -mas derecho
    -mas izquierdo
 No funcionan porque no me deja usar Nodo?
 */


template <class T>
Conjunto<T>::Conjunto(): _raiz(nullptr), _tamano(0){} //, _maximo(nullptr), _minimo(nullptr) {}

template <class T>
Conjunto<T>::~Conjunto() { 
   /* if(hijo derecho != nullptr){
        destruir hijo derecho
    }
    if(hijo izquierdo != nullptr){
        destuir hijo derecho
    }
    destruir el nodo actual con informacion
    */
   if ( _raiz != nullptr ) {
       if (_raiz->der != nullptr)
           destruirHijos(_raiz->der);
       if (_raiz->izq != nullptr)
           destruirHijos(_raiz->izq);
       _raiz->der = nullptr;
       _raiz->izq = nullptr;
       delete _raiz;

       _raiz = nullptr;
       _tamano --;
   }

}

template <class T>
void Conjunto<T>::destruirHijos(Nodo* elem) {
    if ( elem != nullptr ) {
        if (elem->der != nullptr)
            destruirHijos(elem->der);
        if (elem->izq != nullptr)
            destruirHijos(elem->izq);
        elem->izq = nullptr;
        elem->der = nullptr;
        elem->valor = 0;
        delete elem;
        _tamano--;
    }
}


template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
    bool res = false;
    Nodo* raizAux = _raiz;

    while (raizAux != nullptr){
        if (raizAux->valor == clave) {
            res = true;
            raizAux = nullptr;
        }else {
            if (raizAux->valor < clave)
                raizAux = raizAux->der;
            else
                raizAux = raizAux->izq;
        }
    }
    return res;
}

void masIzquierdo(Nodo* sancho){
    //cosas
}


template <class T>
void Conjunto<T>::insertar(const T& clave) {

    if (_raiz == nullptr) {
        _raiz = new Nodo(clave);
        _tamano = 1;
        //_minimo = _raiz;
        //_maximo = _raiz;
    }
    else{
        Nodo* aux = _raiz;
        Nodo* auxPadre = _raiz;
        bool esta = false;

        while (aux != nullptr){
            auxPadre = aux;

            if ( aux->valor == clave ){
                aux = nullptr;
                esta = true;

            } else {
                if (aux->valor < clave)
                    aux = aux->der;
                else
                    aux = aux->izq;
            }
        }

        if( !esta ){
            if (auxPadre->valor < clave ) { // estamos en el caso que aux es nullptr
                auxPadre->der = new Nodo(clave);
                //if ( auxPadre == _maximo )
                 //   _maximo = auxPadre->der;
            }
            else {
                auxPadre->izq = new Nodo(clave); // estamos en el caso que aux es nullptr
               // if ( auxPadre == _minimo )
                //    _minimo = auxPadre->izq;
            }
            _tamano ++;
        }
    }
}


template <class T>
void Conjunto<T>::remover(const T& elem) {  // el mas izquierdo de sus derechos, o el mas derecho de sus izquierdos
    /*necesito
            - el padre del que voy a eliminar : técnica hecha en la busqueda
                - ya que voy a tener que enlazarlo al mas izquierdo/derecho
            - el padre del mas izuiqerdo/derecho : técnica vista en la busqueda
                - ya que cuando "me lo lleve" si tenia hijos debo encgancharlos al padre del mas izquierdo/derecho1
    que hacer
        1 buscamos el elemento a eliminar, guardandonos iteracion a iteracion su padre
        2 una vez encontrado buscamos el mas izquierdo de sus derechos, o el mas derecho de sus izquierdos, guardandonos el padre de ese izq/der
        3 el padre del: mas izq hago que ahora su hijo izq sea el hijo der del mas izq, porque el mas izq no tiene hijo izq, sino no seria el mas izq
                        mas der hago que ahora su hijo der sea el hijo izq del mas der, porque el mas der no tiene hijo der, sino no seria el mas der
        4 los hijos del reemplazador ahora son los hijos del eliminado
        5 delete del eliminado
        6 el padre del eliminado ahora su hijo izq/der es el reemplazador
     */
    Nodo* buscado;
    Nodo* padreDelBuscado;
    padreDelBuscado = nullptr;

    buscado = _raiz;   // Los minimos y maximos?? salen "a mano" por ahora
    bool encontrado= false;

    /// no suponemos que el elemento a elimnar esta en el arbol ///0

    while ( !encontrado && buscado != nullptr ){
        if ( buscado->valor == elem )
            encontrado = true;
        else{
            padreDelBuscado = buscado;
            if ( buscado->valor < elem )
                buscado = buscado->der;
            else
                buscado = buscado->izq;
        }
    }// aca tengo el que tengo que eliminar y su padre
    //queda buscar el mas derecho de sus izq o el mas izq de sus der
    if ( buscado != nullptr ) {
        if (buscado->der != nullptr) { /// buscamos el mas izq de sus der
            Nodo *masIzqDeLosDer = buscado->der;
            Nodo *padreMasIzqDeLosDer = nullptr;
            while (masIzqDeLosDer->izq != nullptr) { /// tenemos al mas izquierdo delos derechos y a su padre
                padreMasIzqDeLosDer = masIzqDeLosDer;
                masIzqDeLosDer = masIzqDeLosDer->izq;

            } /// hay que re-apuntar, ingredientes: buscado, padreDelBuscado, masIzqDeLosDer, padreMasIzqDeLosDer
            if (padreMasIzqDeLosDer == nullptr) { //si el mas izquierdo fuera el derecho del buscado
                masIzqDeLosDer->izq = buscado->izq;  // no cambiamos su .der porque son sus hijos originales
                if ( padreDelBuscado != nullptr ) {
                    if (padreDelBuscado->der == buscado) // el padre del buscado apunta al mas izquierdo
                        padreDelBuscado->der = masIzqDeLosDer;
                    else
                        padreDelBuscado->izq = masIzqDeLosDer;
                }
                if ( buscado == _raiz ){
                    _raiz = masIzqDeLosDer;
                }
                delete buscado;
                _tamano--;
            } else {
                padreMasIzqDeLosDer->izq = masIzqDeLosDer->der; // mas izq .izq es null, sino no seria el mas izq
                masIzqDeLosDer->der = buscado->der;
                masIzqDeLosDer->izq = buscado->izq;
                if ( padreDelBuscado != nullptr ) {
                    if (padreDelBuscado->der == buscado)
                        padreDelBuscado->der = masIzqDeLosDer;
                    else
                        padreDelBuscado->izq = masIzqDeLosDer;
                }
                if ( buscado == _raiz ){
                    _raiz = masIzqDeLosDer;
                }
                delete buscado;
                _tamano--;
            }

        } else {  /// buscamos el mas der de sus izq
            if (buscado->izq != nullptr) {
                Nodo *masDerDeLosIzq = buscado->izq;
                Nodo *padreMasDerDeLosIzq = nullptr;
                while (masDerDeLosIzq->der != nullptr) { /// tenemos al mas derecho delos izquierdos y a su padre
                    padreMasDerDeLosIzq = masDerDeLosIzq;
                    masDerDeLosIzq = masDerDeLosIzq->der;

                } /// hay que re-apuntar, ingredientes: buscado, padreDelBuscado, masDerDeLosIzq, padreMasDerDeLosIzq
                if (padreMasDerDeLosIzq == nullptr) {//si el mas derecho fuera el izquierdo del buscado
                    masDerDeLosIzq->der = buscado->der; // no cambiamos su .izq porque son sus hijos originales
                    if ( padreDelBuscado != nullptr ) {
                        if (padreDelBuscado->der == buscado) // el padre del buscado apunta al mas derecho
                            padreDelBuscado->der = masDerDeLosIzq;
                        else
                            padreDelBuscado->izq = masDerDeLosIzq;
                    }
                    if ( buscado == _raiz ){
                        _raiz = masDerDeLosIzq;
                    }
                    delete buscado;
                    _tamano--;
                } else {
                    padreMasDerDeLosIzq->der = masDerDeLosIzq->izq; // mas der .der es null, sino no seria el mas der
                    masDerDeLosIzq->der = buscado->der;
                    masDerDeLosIzq->izq = buscado->izq;
                    if ( padreDelBuscado != nullptr ) {
                        if (padreDelBuscado->der == buscado)
                            padreDelBuscado->der = masDerDeLosIzq;
                        else
                            padreDelBuscado->izq = masDerDeLosIzq;
                    }
                    if ( buscado == _raiz ){
                        _raiz = masDerDeLosIzq;
                    }
                    delete buscado;
                    _tamano--;
                }
            } else { /// podria ser que sea una hoja, su padre apunte a null y la borramos

                if (padreDelBuscado == nullptr) { // caso raiz unico elemento
                    delete buscado;
                    _raiz = nullptr;
                    _tamano--;
                    //_maximo = nullptr;
                    //_minimo = nullptr;
                } else {
                    if (padreDelBuscado->der == buscado)
                        padreDelBuscado->der = nullptr;
                    else
                        padreDelBuscado->izq = nullptr;

                    delete buscado;
                    _tamano--;
                }
            }
        }
    }
}

    

template <class T>
const T& Conjunto<T>::siguiente(const T& elem) {
    // 1° lo buscamos
    // 2° si tiene hijo derecho, buscamos el mas izquierdo del arbol del hijo derecho
    // 3° si no tiene hijo derecho, subimos hasta encontrar el primer padre mayor al elemento que le estamos buscando el sucesor
    Nodo* res = nullptr;

    Nodo* buscado;
    Nodo* padreDelBuscado;
    padreDelBuscado = nullptr;
    buscado = _raiz;
    bool encontrado= false;
    /// suponemos que el elemento a buscar el siguiente esta en el arbol ///
    while ( !encontrado ){
        if ( buscado->valor == elem )
            encontrado = true;
        else{
            padreDelBuscado = buscado;
            if ( buscado->valor < elem )
                buscado = buscado->der;
            else
                buscado = buscado->izq;
        }
    }// aca tengo el que tengo que buscar el siguiente y a su padre

    if ( buscado->der != nullptr ){ //busco el mas izquierdo de su derecho
        Nodo *masIzqDeLosDer = buscado->der;
        Nodo *padreMasIzqDeLosDer = nullptr;
        while (masIzqDeLosDer->izq != nullptr) { /// tenemos al mas izquierdo delos derechos y a su padre
            padreMasIzqDeLosDer = masIzqDeLosDer;
            masIzqDeLosDer = masIzqDeLosDer->izq;
        }
        res = masIzqDeLosDer;
    }// caso donde tenemos hijo derecho, aca termina

    else{ /// no tiene hijo derecho, tendré que subir hasta encontrar el primer padre mayor
        if (padreDelBuscado->valor > buscado->valor) {
            res = padreDelBuscado;
        }
        else{ //busco un padre mayor al padre

            bool mayor = false;
            Nodo* padrisimo;
            while ( !mayor ) {
                padrisimo = _raiz;
                while (padrisimo->der != padreDelBuscado && padrisimo->izq != padreDelBuscado) { //busco al padre del padre
                    if (padrisimo->valor < padreDelBuscado->valor)
                        padrisimo = padrisimo->der;
                    else
                        padrisimo = padrisimo->izq;
                }
                if ( padrisimo->valor > elem  ) {
                    mayor = true;
                    res = padrisimo;
                }
                else
                    padreDelBuscado = padrisimo;  /// PADRE DEL BUSCADO YA NO SIRVE, NO USARLO~~~~~~~~~~~~~~~~~~~~~~~~~~
            }
        }
    }
    return res->valor;
}


template <class T>
const T& Conjunto<T>::minimo() const {

    /// se asume que no es vacio
    Nodo* min = _raiz;
    if (min != nullptr) {
        while (min->izq != nullptr) {
            min = min->izq;
        }
    }
    return min->valor;
}


template <class T>
const T& Conjunto<T>::maximo() const {

    /// se asume que no es vacio
    Nodo* max = _raiz;
    if (max != nullptr) {
        while (max->der != nullptr) {
            max = max->der;
        }
    }
    return max->valor;
}


template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return _tamano;
}

template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}

/*
template <class T>
void Conjunto<T>::masDerecho(Nodo *elem) {

}
 */
//template <class T>
//Conjunto<T>::Nodo::Nodo(const T &v):valor(v) , izq(nullptr), der(nullptr)  {}