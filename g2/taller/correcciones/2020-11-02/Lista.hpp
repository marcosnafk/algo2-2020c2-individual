#include "Lista.h"

Lista::Lista(): _primero(NULL), _ultimo(NULL) {};

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.

    *this = l;
}

Lista::~Lista() {
    this->_destruir();
}

void Lista::_destruir() {
    while( longitud()>1){ // && _ultimo->anterior != NULL ){
        Nodod* aux = _ultimo->anterior;
        _ultimo->anterior->siguiente = NULL;
        delete _ultimo;
        _ultimo = aux;
    }
    if (longitud()==1) {
        delete _ultimo;
    }
    _ultimo = NULL;
    _primero = NULL;
}

Lista& Lista::operator=(const Lista& aCopiar) {

    this->_destruir();
    Nodod* copiar = aCopiar._primero;  // copiar lo uso para recorrer la lista aCopiar
    if (copiar != NULL) {
        _primero = new Nodod(copiar->valor);
        _ultimo = _primero;
        copiar = copiar->siguiente;
    }
    while (copiar!= NULL){
        int a = copiar->valor;
        _ultimo->siguiente = new Nodod(a); // copiar siempre esta una pos adelante
        _ultimo->siguiente->anterior = _ultimo;
        _ultimo = _ultimo->siguiente;

        copiar = copiar->siguiente;
    }

    return *this;
}

void Lista::agregarAdelante(const int& elem) {
    Nodod* nuevo= new Nodod(elem);
    if (_primero == NULL) {
        _primero = nuevo;
        _ultimo = nuevo;
    }else {
        _primero->anterior = nuevo;
        nuevo->siguiente = _primero;
        _primero = nuevo;
    }
}

void Lista::agregarAtras(const int& elem) {

    Nodod* nuevo= new Nodod(elem);
    if (_primero == NULL) {
        _primero = nuevo;
        _ultimo = nuevo;
    }else{
        _ultimo->siguiente = nuevo;
        nuevo->anterior = _ultimo;
        _ultimo = nuevo;
    }

}

void Lista::eliminar(Nat i) { // supongo que el i esta dentro del tamaño de la lista
    Nodod* aux= _primero;
    for(int j = 0; j <i; j++){
        aux = aux->siguiente;
    }// aux esta en la i-esima posicion

    // enlazar
    if (this-> longitud() == 1){
        _ultimo= NULL;
        _primero= NULL;
    }
    if(aux == _primero & this->longitud()> 1){
        _primero = aux->siguiente;
        aux->siguiente->anterior = NULL;
    }else{
        if (aux == _ultimo & this->longitud()> 1){
            _ultimo = aux->anterior;
            aux->anterior->siguiente = NULL;
        }else{
            if (longitud()!=0) {
                aux->siguiente->anterior = aux->anterior; // asumo aux no es el primero ni el ultimo, entonces long es mayor o igual a 3
                aux->anterior->siguiente = aux->siguiente;
            }
        }
    }
    delete aux;

}

int Lista::longitud() const {
    int result = 0;
    Nodod* aux = _primero;
    while (aux != NULL){
        aux = aux->siguiente;
        result ++;
    }

    return result;
}

const int& Lista::iesimo(Nat i) const { // int& es una referencia, es como el puntero mas simple, en vez de hacer int a int* p = &a, y int c = *p
                                        // acotas con int& c = a, aca con el const no se va apder modificar el valor del nodod
    // Completar
    Nodod* aux= _primero;
    for(int j = 0; j <i< longitud()+1; j++){
        aux = aux->siguiente;
    }// aux esta en la i-esima posicion


    return (aux->valor);
}

int& Lista::iesimo(Nat i) { //aca con el const si se va a poder modificar el valor del nodod
    // Completar (hint: es igual a la anterior...)

    Nodod* aux= _primero;
    for(int j = 0; j <i; j++){
        aux = aux->siguiente;
    }// aux esta en la i-esima posicion

    int a = aux->valor;
    return (aux->valor);
}

void Lista::mostrar(ostream& o) {
    // Completar
    Nodod* pos = _primero;
    o << "< ";
    while (pos->siguiente != NULL && longitud() >1 ){
        o << pos->valor << " , ";
        pos = pos->siguiente;
    }
    if ( longitud()==0) {
        o << " >";
    }else{
        o << pos->valor << " >";
    }
}


ostream &operator<<(ostream &os, Lista &l) {
    l.mostrar(os);
    return os;
}

Lista::Nodod::Nodod(const int &elem): valor(elem), siguiente(NULL), anterior(NULL) {};
