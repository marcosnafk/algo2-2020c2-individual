#include <iostream>

using namespace std;

using uint = unsigned int;

// Ejercicio 1

class Rectangulo {
    public:
        Rectangulo(uint alto, uint ancho);
        uint alto();
        uint ancho();
        float area();

    private:
        uint alto_;
        uint ancho_;

};

Rectangulo::Rectangulo(uint alto, uint ancho) : alto_(alto), ancho_(ancho)  {};    //Cosntructor

uint Rectangulo::alto() {
    return this -> alto_;
}

uint Rectangulo:: ancho() {
    return this -> ancho_;
}

float Rectangulo::area() {
    return this -> alto_ * ancho_;
}

// Ejercicio 2

class Elipse{

    public:
        Elipse(uint a, uint b);
        uint r_a();
        uint r_b();
        float PI = 3.14;
        float area();
    private:
        uint r_a_;
        uint r_b_;
};

Elipse::Elipse(uint a, uint b): r_a_(a), r_b_(b) {};

uint Elipse:: r_a() {
    return this -> r_a_;
}

uint Elipse:: r_b() {
    return this -> r_b_;
}

float Elipse:: area() {
    return this -> PI * r_a_ * r_b_;
}

// Ejercicio 3

class Cuadrado {
    public:
        Cuadrado(uint lado);
        uint lado();
        float area();

    private:
        Rectangulo r_;
};

Cuadrado::Cuadrado(uint lado): r_(lado, lado) {}

uint Cuadrado::lado() {
    return this -> r_.alto();
}

float Cuadrado::area() {
    return this -> r_.area();
}

// Ejercicio 4

class Circulo{
    public:
        Circulo(uint radio);
        uint radio();
        float area();

    private:
        Elipse radio_;
};

Circulo::Circulo(uint radio): radio_(radio, radio)  {}

uint Circulo:: radio() {
    return this -> radio_.r_b();
}

float Circulo::area() {
    return this -> radio_.area();
}


// Ejercicio 5

ostream& operator<<(ostream& os, Rectangulo r) {
    os << "Rect(" << r.alto() << ", " << r.ancho() << ")";
    return os;
}

ostream& operator <<(ostream& os, Elipse eli1){
    os << "Elipse(" << eli1.r_a() <<", " << eli1.r_b() << ")";
    return os;
}

// Ejercicio 6

ostream& operator<<(ostream& os, Cuadrado cuad){
    os << "Cuad(" << cuad.lado() << ")";
    return os;
}

ostream& operator<<(ostream& os, Circulo cir ){
    os << "Circ(" << cir.radio() << ")";
    return os;
}
