#include <iostream>
#include <list>
#include <map>
#include <set>
#include <vector>

using namespace std;

using uint = unsigned int;

// Pre: 0 < mes <= 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
  public:
    Fecha(int mes, int dia);
    int mes();
    int dia();

    //Ej9
    bool operator==(Fecha o);
    bool operator <(Fecha o);

    //Ej10
    void incrementar_dia();


  private:
    int mes_;
    int dia_;
    //Completar miembros internos
};

Fecha::Fecha(int mes, int dia): mes_(mes), dia_(dia)  {}

int Fecha::mes() {
    return this -> mes_;
}
int Fecha::dia(){
    return this -> dia_;
}
//Ej10

void Fecha::incrementar_dia() {
    if ( dia_ < dias_en_mes(mes_) ){
        dia_ ++;
    }else{
        dia_ = 1;
        if ( mes_ < 12) {
            mes_++ ;
        }else {
            mes_ = 1;
        }
    }

}


ostream& operator<<(ostream& os,Fecha evento){
    os << evento.dia() << "/" << evento.mes();
    return os;
}

//Ej9

bool Fecha:: operator==(Fecha s){
    return mes_ == s.mes() and dia_ == s.dia();
}
bool Fecha:: operator<(Fecha s){
    bool result = 0;
    if (mes_ < s.mes()){
        result = 1;
    }else{
        if (mes_ > s.mes()){
            result = 0;
        }else{
            result = dia_ < s.dia();
        }
    }
    return result;

}


// Ejercicio 11, 12}


class Horario{
public:
    Horario(uint hora, uint min);
    uint hora();
    uint min();
    bool operator==(Horario h2);
    bool operator<(Horario h2);

private:
    uint hora_;
    uint  min_;
};

bool Horario::operator==(Horario h2) {
    return hora_ == h2.hora() and min_ == h2.min();
}

bool Horario::operator<(Horario h2){
    bool res= 0;
    if (hora_ < h2.hora()){
        res = true;
    }else{
        if (hora_ > h2.hora()){
            res = false;
        }else{
            res = min_ < h2.min();
        }
    }
    return res;
}

ostream& operator<<(ostream& os, Horario h1){
    os << h1.hora() << ":" << h1.min();
    return os;
}

Horario:: Horario(uint hora, uint min): hora_(hora), min_(min)  {}

uint Horario::hora() {
    return this -> hora_;
}
uint Horario::min() {
    return this -> min_;
}

// Ejercicio 13

class Recordatorio{
public:
    Recordatorio(Fecha fecha, Horario hora, string evento);
    Fecha diaDelReco();
    Horario horaDelReco();
    string cualEvento();

private:
    Fecha fecha_;
    Horario hora_;
    string evento_;
};

Recordatorio::Recordatorio(Fecha fecha, Horario hora, string evento): fecha_(fecha), hora_(hora), evento_(evento) {}

Fecha Recordatorio::diaDelReco() {
    return fecha_;
}
Horario Recordatorio:: horaDelReco() {
    return hora_;
}

string Recordatorio::cualEvento() {
    return evento_;
}

ostream& operator<<(ostream& os, Recordatorio reco){
    os << reco.cualEvento()  << " @ " << reco.diaDelReco() << " "  << reco.horaDelReco();
    return os;
}


// Ejercicio 14

class Agenda{
public:
    Agenda(Fecha fecha_inicial);
    void agregar_recordatorio(Recordatorio rec);
    void incrementar_dia();
    list<Recordatorio> recordatorios_de_hoy();
    Fecha hoy();

private:
   vector<Recordatorio> agenda_;
    Fecha diaHoy_;


};
Agenda::Agenda(Fecha fecha_inicial): diaHoy_(fecha_inicial), agenda_()  {}

Fecha Agenda::hoy() {
    return diaHoy_;
}


void Agenda::agregar_recordatorio(Recordatorio rec) {
    agenda_.push_back(rec);
}

void Agenda::incrementar_dia() {
    diaHoy_.incrementar_dia();
}


list<Recordatorio> Agenda:: recordatorios_de_hoy() {

    for (int i = 0; i < agenda_.size()-1 ; ++i) {  //ordeno los recordatorios del mismo dia de menor a mayor horario
        if (agenda_[i].diaDelReco() == diaHoy_) {
            int min = i;
            for (int j = i + 1; j < agenda_.size(); ++j) {
                if (agenda_[j].diaDelReco() == diaHoy_ && agenda_[j].horaDelReco() < agenda_[min].horaDelReco())
                    min = j;
            }
            Recordatorio aux = agenda_[i];
            agenda_[i] = agenda_[min];
            agenda_[min] = aux;
        }
    }
    list<Recordatorio> devolver;
    for (int i = 0; i < agenda_.size() ; ++i) {       //los metemos a una list
        if ( agenda_[i].diaDelReco() == diaHoy_ )
            devolver.push_back( agenda_[i] );
    }

    return devolver;
}


ostream& operator<<(ostream& os, Agenda a1) {
    os << a1.hoy() ;
    os << std::endl;
    os << "=====" ;
    os << std::endl;

    for(Recordatorio reco : a1.recordatorios_de_hoy()){  //supogno que en una list los agarra por orden de first a end
        os << reco << std::endl;
    }

    return os;
}
